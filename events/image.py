import requests


url = "https://api.github.com/some/endpoint"
header = {
    "Authorization": "lcAxBGbKAFGhUlzfN68BvzFyeQJCOTGQhuMtsAWsqY02qv5t3yCOKT9r"
}

response = requests.get(url, headers=header)

data = response.json()

photo = {"original_url": data["photos"][0]["src"]["original"]}
